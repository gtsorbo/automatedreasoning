package advancedPropInference;

import pl.core.Conjunction;
import pl.core.Disjunction;
import pl.core.Implication;
import pl.core.KB;
import pl.core.Negation;
import pl.core.Symbol;

public class LiarsTruthTellersWalkSAT extends KB {

	public LiarsTruthTellersWalkSAT() {
		super();
		Symbol a = intern("amy");
		Symbol b = intern("bob");
		Symbol c = intern("cal");
		
		add(new Implication(a, new Conjunction(c, a)));
		add(new Implication(b, new Negation(c)));
		add(new Implication(c, new Disjunction(b, new Negation(a))));
	}
	
	public static void main(String[] argv) {
		LiarsTruthTellersWalkSAT kb = new LiarsTruthTellersWalkSAT();
		Symbol amy = kb.intern("amy");
		Symbol bob = kb.intern("bob");
		Symbol cal = kb.intern("cal");
		WalkSAT eval = new WalkSAT();
		if(eval.walkSAT(kb, amy, 0.5, 1000) != null) {
			System.out.println("The KB entails the given sentence");
		} else System.out.println("The KB DOES NOT entail the given sentence");
		if(eval.walkSAT(kb, bob, 0.5, 1000) != null) {
			System.out.println("The KB entails the given sentence");
		} else System.out.println("The KB DOES NOT entail the given sentence");
		if(eval.walkSAT(kb, cal, 0.5, 1000) != null) {
			System.out.println("The KB entails the given sentence");
		} else System.out.println("The KB DOES NOT entail the given sentence");
	}
}