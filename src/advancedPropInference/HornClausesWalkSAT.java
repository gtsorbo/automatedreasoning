package advancedPropInference;

import java.util.Set;

import pl.cnf.Clause;
import pl.core.Conjunction;
import pl.core.Disjunction;
import pl.core.Implication;
import pl.core.KB;
import pl.core.Negation;
import pl.core.Sentence;
import pl.core.Symbol;
import pl.util.ArraySet;

public class HornClausesWalkSAT extends KB {

	//Set<Clause> clauses;
	
	public HornClausesWalkSAT() {
		super();
		
		//clauses = new ArraySet<>();
		
		Symbol mythical = intern("mythical");
		Symbol immortal = intern("immortal");
		Symbol mammal = intern("mammal");
		Symbol mortal = intern("mortal");
		Symbol horned = intern("horned");
		Symbol magical = intern("magical");
		
		add(new Implication(mythical, immortal));
		add(new Implication(new Negation(mythical), new Conjunction(mortal, mammal)));
		add(new Implication(new Disjunction(immortal, mammal), horned));
		add(new Implication(horned, magical));
		
/*		Clause c1 = new Clause(new Disjunction(new Negation(mythical), immortal));
		Clause c2 = new Clause(new Disjunction(new Negation(horned), magical));
		Clause c3 = new Clause(new Disjunction(mythical, mortal));
		Clause c4 = new Clause(new Disjunction(mythical, mammal));
		Clause c5 = new Clause(new Disjunction(new Negation(immortal), horned));
		Clause c6 = new Clause(new Disjunction(new Negation(mammal), horned));
		
		clauses.add(c1);
		clauses.add(c2);
		clauses.add(c3);
		clauses.add(c4);
		clauses.add(c5);
		clauses.add(c6); */
	}
	
	public static void main(String[] argv) {
		HornClausesWalkSAT kb = new HornClausesWalkSAT();
		Sentence s1 = kb.intern("mythical");
		Sentence s2 = kb.intern("magical");
		Sentence s3 = kb.intern("horned");
		WalkSAT eval = new WalkSAT();
		
		//kb.clauses.add(new Clause(s1));
		if(eval.walkSAT(kb, s1, 0.5, 100) != null) {
			System.out.println("The KB entails the given sentence");
		} else System.out.println("The KB DOES NOT entails the given sentence");
		//kb.clauses.remove(new Clause(s1));
		
		//kb.clauses.add(new Clause(s2));
		if(eval.walkSAT(kb, s2, 0.5, 100) != null) {
			System.out.println("The KB entails the given sentence");
		} else System.out.println("The KB DOES NOT entails the given sentence");
		//kb.clauses.remove(new Clause(s2));
		
		//kb.clauses.add(new Clause(s3));
		if(eval.walkSAT(kb, s3, 0.5, 100) != null) {
			System.out.println("The KB entails the given sentence");
		} else System.out.println("The KB DOES NOT entails the given sentence");
		//kb.clauses.remove(new Clause(s3));
	}
}
