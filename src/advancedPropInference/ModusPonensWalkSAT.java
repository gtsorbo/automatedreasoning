package advancedPropInference;

import java.util.Set;

import pl.core.*;

public class ModusPonensWalkSAT extends KB {
	
	public ModusPonensWalkSAT() {
		super();
		Symbol p = intern("P");
		Symbol q = intern("Q");
		add(p);
		add(new Implication(p, q));
	}
	
	public static void main(String[] argv) {
		ModusPonensWalkSAT kb = new ModusPonensWalkSAT();
		Symbol s = kb.intern("Q");
		WalkSAT walk = new WalkSAT();
		
		// test if Q is true based on KB
		if(walk.walkSAT(kb, s, 0.5, 100) != null) {
			System.out.println("The KB entails the given sentence");
		}
		else System.out.println("The KB DOES NOT entails the given sentence.");
	}

}
