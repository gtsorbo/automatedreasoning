package advancedPropInference;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Random;
import java.util.Set;

import pl.cnf.CNFConverter;
import pl.cnf.Clause;
import pl.core.KB;
import pl.core.Model;
import pl.core.Sentence;
import pl.core.Symbol;

public class WalkSAT {

	Random rand = new Random();

	public Model walkSAT(KB kb, Sentence sent, double p, int maxFlips) {
		// add clauses from knowledge base
		Set<Clause> clauses = CNFConverter.convert(kb);
		// add clauses from sentence
		clauses.addAll(CNFConverter.convert(sent));
		
		Model model = randomAssignmentToModel(kb.symbols());
		
		for(int i=0; i<maxFlips; i++) {
			if(modelSatisfiesClauses(model, clauses)) {
				return model;
			}
			Clause falseClause = getFalseClause(model, clauses);
			if(rand.nextDouble() < p) {
				model = flipRandomSymbolFromClause(falseClause, model);
			}
			else {
				model = minConflictSymbol(falseClause, clauses, model);
			}
		}
		return null;
	}
	
	public Model walkSAT(Set<Clause> clauses, double p, int maxFlips) {
		
		ArrayList<Symbol> symbols = new ArrayList<>();
		
		for(Clause c : clauses) {
			for(Symbol s : c.getSymbols()) {
				if(!symbols.contains(s)) {
					symbols.add(s);
				}
			}
		}
		
		Model model = randomAssignmentToModel(symbols);
		
		for(int i=0; i<maxFlips; i++) {
			if(modelSatisfiesClauses(model, clauses)) {
				return model;
			}
			Clause falseClause = getFalseClause(model, clauses);
			if(rand.nextDouble() < p) {
				model = flipRandomSymbolFromClause(falseClause, model);
			}
			else {
				model = minConflictSymbol(falseClause, clauses, model);
			}
		}
		return null;		
	}
	
	private Model minConflictSymbol(Clause falseClause, Set<Clause> clauses, Model model) {
		Model updatedModel = model;
		ArrayList<Symbol> symbols = falseClause.getSymbols();
		
		int maxClausesSatisfied = -1;
		
		// symbols in clause
		for(Symbol s : symbols) {
			Model modelWithSymbolFlipped = updatedModel;
			modelWithSymbolFlipped.flipSymbol(s);
			int numSatisfiedClauses = 0;
			for(Clause clause : clauses) {
				if(clause.isSatisfiedBy(modelWithSymbolFlipped)) {
					numSatisfiedClauses++;
				}
			}
			
			if(numSatisfiedClauses > maxClausesSatisfied) {
				updatedModel = modelWithSymbolFlipped;
				maxClausesSatisfied = numSatisfiedClauses;
				if(numSatisfiedClauses == clauses.size()) {
					// all clauses satisfied!
					break;
				}
			}
		}
		return updatedModel;
	}

	private Model flipRandomSymbolFromClause(Clause c, Model m) {
		Model flipModel = m;
		int index = rand.nextInt(c.size());
		Symbol randSym = c.getSymbols().get(index);
		flipModel.flipSymbol(randSym);
		return flipModel;
	}

	private Model randomAssignmentToModel(Collection<Symbol> sym) {
		Model m = new Model(sym);
		for(int i=0; i<m.getSymbolCount(); i++) {
			Symbol s = m.getSymbolAt(i);
			m.set(s, rand.nextBoolean());
		}
		return m;
	}
	
	private boolean modelSatisfiesClauses(Model m, Set<Clause> clauses) {
		for(Clause c : clauses) {
			if(!c.isSatisfiedBy(m)) {
				return false;
			}
		}
		return true;
	}
	
	private Clause getFalseClause(Model m, Set<Clause> clauses) {
		for(Clause c : clauses) {
			if(!c.isSatisfiedBy(m)) {
				return c;
			}
		}
		return null;
	}
}
