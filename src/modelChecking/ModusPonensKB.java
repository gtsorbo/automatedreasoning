package modelChecking;

import pl.core.*;

public class ModusPonensKB extends KB {
	
	public ModusPonensKB() {
		super();
		Symbol p = intern("P");
		Symbol q = intern("Q");
		add(p);
		add(new Implication(p, q));
	}
	
	public static void main(String[] argv) {
		ModusPonensKB kb = new ModusPonensKB();
		//Conjunction conj = new Conjunction(kb.intern("P"), new Negation(kb.intern("Q")));
		Symbol s = kb.intern("Q");
		TTEntails entails = new TTEntails();
		System.out.println(entails.ttEntails(kb, s));
	}

}
