package modelChecking;

import pl.core.Conjunction;
import pl.core.Disjunction;
import pl.core.Implication;
import pl.core.KB;
import pl.core.Negation;
import pl.core.Sentence;
import pl.core.Symbol;

public class HornClausesKB extends KB {

	public HornClausesKB() {
		super();
		Symbol mythical = intern("mythical");
		Symbol immortal = intern("immortal");
		Symbol mammal = intern("mammal");
		Symbol mortal = intern("mortal");
		Symbol horned = intern("horned");
		Symbol magical = intern("magical");
		
		add(new Implication(mythical, immortal));
		add(new Implication(new Negation(mythical), new Conjunction(mortal, mammal)));
		add(new Implication(new Disjunction(immortal, mammal), horned));
		add(new Implication(horned, magical));
	}
	
	public static void main(String[] argv) {
		HornClausesKB kb = new HornClausesKB();
		Sentence s1 = kb.intern("mythical");
		Sentence s2 = kb.intern("magical");
		Sentence s3 = kb.intern("horned");
		TTEntails entails = new TTEntails();
		System.out.println(entails.ttEntails(kb, s1));
		System.out.println(entails.ttEntails(kb, s2));
		System.out.println(entails.ttEntails(kb, s3));
	}
}
