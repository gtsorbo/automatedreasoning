package modelChecking;

import java.util.ArrayList;
import pl.core.*;

public class TTEntails {

	public boolean ttEntails(KB kb, Sentence alpha) {
		ArrayList<Symbol> symbols = new ArrayList<>(kb.symbols());
		
		return ttCheckAll(kb, alpha, symbols, 0, new Model(symbols));
	}
	
	public boolean ttCheckAll(KB kb, Sentence alpha, ArrayList<Symbol> symbols, int index, Model model) {
		if(index == symbols.size()) {
			if(model.satisfies(kb)) {
				return model.satisfies(alpha);
			}
			else return true;
		}
		else {
			return ttCheckAll(kb, alpha, symbols, index+1, model.union(symbols.get(index), true)) 
					&& ttCheckAll(kb, alpha, symbols, index+1, model.union(symbols.get(index), false));
		}
		
	}
}