package pl.core;

import java.util.ArrayList;
import java.util.Collection;

public class Model implements AbstractModel {

	ArrayList<Symbol> symbols;
	boolean[] values;
	
	public Model(Collection<Symbol> symbols) {
		this.symbols = new ArrayList<>(symbols);
		values = new boolean[symbols.size()];
	}
	
	public Model union(Symbol s, boolean val) {
		Model m = new Model(this.symbols);
		for(int i=0; i<m.symbols.size(); i++) {
			m.values[i] = this.values[i];
		}
		m.set(s, val);
		return m;
	}
	
	@Override
	public void set(Symbol sym, boolean value) {
		int index = symbols.indexOf(sym);
		if(index < 0) {
			System.out.println("Error: invalid symbol assignment");
		}
		else values[index] = value;
	}

	@Override
	public boolean get(Symbol sym) {
		int index = symbols.indexOf(sym);
		if(index < 0) {
			System.out.println("Error: symbol not found");
			return false;
		}
		else return values[index];
	}

	@Override
	public boolean satisfies(KB kb) {
		for(Sentence s : kb.sentences) {
			if(!s.isSatisfiedBy(this)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public boolean satisfies(Sentence sentence) {
		return sentence.isSatisfiedBy(this);
	}
	
	public int getSymbolCount() {
		return symbols.size();
	}
	
	public Symbol getSymbolAt(int index) {
		return symbols.get(index);
	}
	
	public void flipSymbol(Symbol s) {
		values[symbols.indexOf(s)] = !values[symbols.indexOf(s)];
	}

	@Override
	public void dump() {
		for (Symbol s : symbols) {
			System.out.println(s + ": " + values[symbols.indexOf(s)]);
		}
		
	}
}
